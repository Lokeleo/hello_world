package com.example.demo.Repository;

import com.example.demo.Entity.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface HistoryRepository extends JpaRepository<History,Long> {
    @Query("SELECT c FROM History c WHERE c.iduser=?1 AND c.idmanga=?2")
    History findByIduserAndIdManga(Long id_user,Long id_manga);

    @Query("SELECT c.idmanga FROM History c WHERE c.iduser=?1 order by c.day DESC ,c.time DESC ")
    List<Long> findByIduser(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM History c WHERE c.iduser=?1 OR c.idmanga=?2")
    void deleteHistoryByIdUserOrIdManga(Long iduser,Long idmanga);
    @Transactional
    @Modifying
    @Query("DELETE FROM History c WHERE c.idmanga=?1")
    void deleteHistoryByIdManga(Long idmanga);

}
