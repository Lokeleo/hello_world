package com.example.demo.Repository;

import com.example.demo.Entity.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ChapterRepository extends JpaRepository<Chapter,Long> {
    @Query("SELECT c FROM Chapter c WHERE c.idmanga=?1 AND c.id=?2")
    Chapter findByChapterandID(Long idmanga,Long id);

    @Query("SELECT c FROM Chapter c WHERE c.idmanga=?1 AND c.number=?2")
    Chapter findByNumberandID(Long idmanga,int number);

    @Query("SELECT c FROM Chapter c WHERE c.idmanga=?1 ORDER BY c.id")
    List<Chapter> showAllChapterById(Long id);

    @Query("SELECT c FROM Chapter c ORDER BY c.id")
    List<Chapter> showAllChapter();

    @Query("SELECT c FROM Chapter c WHERE c.id=?1")
    Chapter findChapteById(Long id);

    @Query("SELECT c FROM Chapter c WHERE c.idmanga=?1 AND c.nextchap = c.id")
    Chapter findNewChapter(Long id_manga);

    @Query("SELECT c FROM Chapter c WHERE c.idmanga=?1 AND c.prechap = c.id")
    Chapter findFirstChapter(Long id_manga);

    @Query("SELECT MAX(c.id) FROM Chapter c")
    Long findMaxId();

    @Transactional
    @Modifying
    @Query("DELETE FROM Chapter c WHERE c.id=?1")
    void deleteChapterById(Long id);
}
