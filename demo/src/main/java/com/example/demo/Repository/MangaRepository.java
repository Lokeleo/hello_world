package com.example.demo.Repository;

import com.example.demo.Entity.ListManga;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface MangaRepository extends JpaRepository<ListManga,Long> {
    @Query("SELECT c FROM ListManga c WHERE c.IDuser=?1")
    List<ListManga> findByIDuser(Long id);

    @Query("SELECT c FROM ListManga c WHERE c.name=?1")
    ListManga findByName(String name);

    @Query("SELECT c FROM ListManga c WHERE c.ID=?1")
    ListManga findByID(Long ID);

    @Transactional
    @Modifying
    @Query("DELETE FROM ListManga c WHERE c.ID=?1")
    void deleteMangaById(Long id);

    @Query("SELECT c FROM ListManga c WHERE c.name like %?1% OR c.content like %?1%")
    List<ListManga> findManga(String name);

    @Query("SELECT c FROM ListManga c ORDER BY c.daycreate,c.timecreate DESC ")
    List<ListManga> showAllManga();

    @Query("SELECT c FROM ListManga c ORDER BY c.count_view DESC ")
    List<ListManga> showHotManga();
}
