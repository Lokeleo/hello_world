package com.example.demo.Repository;

import com.example.demo.Entity.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
public interface FavoriteRepository extends JpaRepository<Favorite,Long> {
    @Query("SELECT c FROM Favorite c WHERE c.iduser=?1 AND c.idmanga=?2")
    Favorite findByIdUserAndIdManga(Long iduser,Long idmanga);

    @Transactional
    @Modifying
    @Query("DELETE FROM Favorite c WHERE c.iduser=?1 AND c.idmanga=?2")
    void deleteFavoriteByIdUserAndIdManga(Long iduser,Long idmanga);

    @Transactional
    @Modifying
    @Query("DELETE FROM Favorite c WHERE c.iduser=?1 OR c.idmanga=?2 ")
    void deleteFavoriteByIdUserOrIdManga(Long iduser,Long idmanga);

    @Query("SELECT c.idmanga FROM Favorite c WHERE c.iduser=?1 ORDER BY c.day DESC, c.time DESC ")
    List<Long> findListMangaFavorite(Long id);
}
