package com.example.demo.Repository;

import com.example.demo.Entity.Manga;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Repository
public interface PictureMangaRepository extends JpaRepository<Manga, Long> {
    @Query("SELECT c FROM Manga c WHERE c.link=?1")
    Manga findByPhotos(String photos);

    @Query("SELECT c FROM Manga c WHERE c.ID=?1")
    Manga findpictureByID(Long id);

    @Query("SELECT c FROM Manga c WHERE c.Idchapter =?1 order by c.ID")
    List<Manga> showManga(Long ID);
    @Transactional
    @Modifying
    @Query("DELETE FROM Manga c WHERE c.ID=?1")
    void deleteOnepageById( Long id);
}
