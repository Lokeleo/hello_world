package com.example.demo.Repository;

import com.example.demo.Entity.Type_manga;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface Type_mangaRepository extends JpaRepository<Type_manga,Long> {
    @Query("SELECT c.idmanga FROM Type_manga c WHERE c.idtype=?1")
    List<Long> findByCategory(Long id);

//    @Transactional
//    @Modifying
//    @Query("DELETE FROM Type_manga c WHERE c.idmanga=?1")
//    void deleteByIdMangaAndIdCategory(Long idmanga);

    @Transactional
    @Modifying
    @Query("DELETE FROM Type_manga c WHERE c.idmanga=?1")
    void deleteByIdManga(Long idmanga);
}
