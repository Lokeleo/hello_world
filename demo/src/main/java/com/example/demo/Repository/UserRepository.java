package com.example.demo.Repository;

import com.example.demo.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT c FROM User c WHERE c.username=?1 AND c.password=?2")
    User findByUsernameAndPassword(String username, String password);
    @Query("SELECT c FROM User c WHERE c.email = ?1")
    User findByEmail(String email);
    @Query("SELECT c FROM User c WHERE c.ID=?1")
    User findByID(Long ID);
    User findByUsername(String username);
    User findByResetPasswordToken(String token);
    @Query("SELECT c FROM User c WHERE c.ID=?1")
    User findUserById(Long id);

    @Query("SELECT c FROM User c")
    List<User> showAllUser();

    @Transactional
    @Modifying
    @Query("DELETE FROM User c WHERE c.ID = ?1")
    void deleteUserByAdmin(Long id);
}
