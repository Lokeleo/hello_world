package com.example.demo.controller;

import com.example.demo.Entity.*;
import com.example.demo.Repository.*;
import com.example.demo.photos.userID.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
public class Authorcontroller {
    @Autowired
    private PictureMangaRepository mangarepo;
    @Autowired
    private MangaRepository listmangarepo;
    @Autowired
    private ChapterRepository chapterrepo;
    @Autowired
    HistoryRepository historyRepository;
    @Autowired
    Type_mangaRepository type_mangaRepository;

    public void deleteDir(File file) {
        if (file.isDirectory()) {
            String[] files = file.list();
            for (String child : files) {
                File childDir = new File(file, child);
                if (childDir.isDirectory()) {
                    deleteDir(childDir);
                } else {
                    childDir.delete();
                }
            }
            if (file.list().length == 0) {
                file.delete();
            }

        } else {
            file.delete();
        }
    }
    public void deleteonepage(Long id_page){

        mangarepo.deleteOnepageById(id_page);
    }
    public void deletemanga(Long id_manga){
        listmangarepo.deleteMangaById(id_manga);
        List<Chapter> chapters = chapterrepo.showAllChapterById(id_manga);
        historyRepository.deleteHistoryByIdManga(id_manga);
        type_mangaRepository.deleteByIdManga(id_manga);
        for(Chapter x:chapters){
            deletechapter(x.getId());
        }
    }
    public void deletechapter(Long id_chapter){
        chapterrepo.deleteChapterById(id_chapter);
        List<Manga> list = mangarepo.showManga(id_chapter);
        for(Manga x:list){
            deleteonepage(x.getID());
        }
    }

    @PostMapping("deletemanga")
    public String deleteManga(HttpServletRequest request, Model model){
        String name = request.getParameter("name_manga");
        return "listcreatemanga";
    }

    @RequestMapping("addnewmanga")
    public String addNewManga(Model model, HttpServletRequest request){
        HttpSession session = request.getSession();
        if(session.getAttribute("id") == null){
            model.addAttribute("user",new User());
            model.addAttribute("message","You need login to user function!!!");
            return "/login";
        }
        model.addAttribute("id",session.getAttribute("id"));
        return "createmanga";
    }

    @RequestMapping("addnewchapter")
    public String new_chapter(Model model, HttpServletRequest request){
        HttpSession session = request.getSession();
        if(session.getAttribute("id") == null){
            model.addAttribute("user",new User());
            model.addAttribute("message","You need login to user function!!!");
            return "/login";
        }
        model.addAttribute("id",session.getAttribute("id"));
        return "createchapter";
    }

    @RequestMapping(value="selectmanga_author")
    public String select_manga( HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        String id = request.getParameter("id_manga");
        List<Chapter> list = chapterrepo.showAllChapterById(Long.parseLong(id));
        session.setAttribute("id_manga",id);
        if(list.isEmpty()){
            model.addAttribute("message", "Not available yet");
        }
        model.addAttribute("id",session.getAttribute("id"));
        model.addAttribute("idmanga",id);
        model.addAttribute("chapters",list);
        return "chaptermanager";
    }
    @RequestMapping("selectchapter_author")
    public String selectChapter(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        String chapter = request.getParameter("id_chapter");
        if(chapter.equals("0")) {
            chapter = request.getParameter("test");
        }
        String id_manga = session.getAttribute("id_manga").toString();
        List<Manga> list = mangarepo.showManga(Long.parseLong(chapter));
        List<Chapter> chapters = chapterrepo.showAllChapterById(Long.parseLong(id_manga));
        Chapter chap = chapterrepo.findByChapterandID(Long.parseLong(id_manga), Long.parseLong(chapter));
        model.addAttribute("idmanga", id_manga);
        model.addAttribute("idchapter", chapter);
        model.addAttribute("mangas", list);
        model.addAttribute("chapter", chap);
        model.addAttribute("chapters", chapters);
        model.addAttribute("id",session.getAttribute("id"));
        return "setupchapter";
    }

    @PostMapping("/delete_page")
    public String deletePageMangaByAuthor(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        String id_delete = request.getParameter("id");
        Manga pagedelete = mangarepo.findpictureByID(Long.parseLong(id_delete));
        deleteonepage(Long.parseLong(id_delete));
        selectChapter(request,model);
        String currentDirectory = System.getProperty("user.dir");
        File file = new File(currentDirectory + pagedelete.getLink(session.getAttribute("id_manga").toString()));
        deleteDir(file);
        return "setupchapter";
    }
    @PostMapping("/delete_chapter")
    public String deleteChapterByAuthor(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        String id_delete = request.getParameter("id_chapter");
        String id_manga = session.getAttribute("id_manga").toString();
        Chapter chapter = chapterrepo.findChapteById(Long.parseLong(id_delete));

        String currentDirectory = System.getProperty("user.dir");
        File file = new File(currentDirectory + "/manga/" + id_manga+"/"+id_delete);
        deleteDir(file);
        if(chapter.getId() == chapter.getPrechap()){
            Chapter chapter_First = chapterrepo.findChapteById(chapter.getNextchap());
            chapter_First.setPrechap(chapter_First.getId());
            chapterrepo.save(chapter_First);
        } else if (chapter.getId() == chapter.getNextchap()){
            Chapter chapter_Last = chapterrepo.findChapteById(chapter.getPrechap());
            chapter_Last.setNextchap(chapter_Last.getId());
            chapterrepo.save(chapter_Last);
        } else {
            Chapter chapter_next = chapterrepo.findChapteById(chapter.getNextchap());
            Chapter chapter_prev = chapterrepo.findChapteById(chapter.getPrechap());
            chapter_next.setPrechap(chapter_prev.getId());
            chapter_prev.setNextchap(chapter_next.getId());
            chapterrepo.saveAll(Arrays.asList(chapter_prev,chapter_next));
        }
        deletechapter(Long.parseLong(id_delete));
        ListManga manga = listmangarepo.findByID(chapter.getIdmanga());
        manga.setCountchapter(manga.getCountchapter()-1);
        listmangarepo.save(manga);
        select_manga(request,model);
        return "chaptermanager";
    }
    @PostMapping("/delete_manga")
    public String deleteMangaByAuthor(HttpServletRequest request,Model model){
        String currentDirectory = System.getProperty("user.dir");
        String id_delete = request.getParameter("id");
        File file = new File(currentDirectory+"/manga/"+id_delete);
        deleteDir(file);
        deletemanga(Long.parseLong(id_delete));
        manga_mangager(request,model);
        return "listcreatemanga";
    }

    @RequestMapping("createmanga")
    public String manga_mangager(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        if(session.getAttribute("id") == null){
            model.addAttribute("user",new User());
            model.addAttribute("message","You need login to user function!!!");
            return "/login";
        }
        String id_user = session.getAttribute("id").toString();
        List<ListManga> list = listmangarepo.findByIDuser(Long.parseLong(id_user));
        model.addAttribute("list",list);
        model.addAttribute("id",id_user);
        return "listcreatemanga";
    }

    @PostMapping(value = "create")
    public String create_new_manga(HttpServletRequest request, @RequestParam("image") MultipartFile file,
                                 @RequestParam(value = "type",required = false) String[] checkboxValue,Model model) throws IOException {
        HttpSession session = request.getSession();
        String id = session.getAttribute("id").toString();
        String name = request.getParameter("name");
        model.addAttribute("id",session.getAttribute("id"));
        if(checkboxValue==null){
            String id_user = session.getAttribute("id").toString();
            model.addAttribute("id",id_user);
            model.addAttribute("message","Category is not null!!!");
            return "createmanga";
        }
        if(file.isEmpty()){
            model.addAttribute("message","File is empty!!!");
            return "createmanga";
        }else {
            if (!Objects.nonNull(listmangarepo.findByName(name))) {
                String content = request.getParameter("content");
                String fileName = StringUtils.cleanPath(file.getOriginalFilename());
                ListManga newmanga = listmangarepo.save(new ListManga(Long.parseLong(id),
                        name,0, fileName, content, java.time.LocalDate.now(), java.time.LocalTime.now()));
                String uploadDir = "manga/" + newmanga.getID();
                FileUploadUtil.saveFile(uploadDir, fileName, file);
                if(checkboxValue.length!=0){
                    for(String x:checkboxValue){
                        type_mangaRepository.save(new Type_manga(newmanga.getID(),Long.parseLong(x)));
                    }
                }

            }else{
                model.addAttribute("message","Name manga is exist!!!");
                return "createmanga";
            }
        }
        String id_user = session.getAttribute("id").toString();
        List<ListManga> list = listmangarepo.findByIDuser(Long.parseLong(id_user));
        model.addAttribute("list",list);
        return "listcreatemanga";
    }

    @PostMapping("createnewchapter")
    public String createnewchapter(HttpServletRequest request,Model model,@RequestParam("files") MultipartFile[] files) throws IOException {
        HttpSession session = request.getSession();
        String id = session.getAttribute("id").toString();
        String id_manga = session.getAttribute("id_manga").toString();
        List<Chapter> listchapter = chapterrepo.showAllChapterById(Long.parseLong(id_manga));
        List<Manga> list = new ArrayList<>();
        String name = request.getParameter("content");
        String chap = request.getParameter("number");
        Chapter chapter = chapterrepo.findByNumberandID(Long.parseLong(id_manga),Integer.parseInt(chap));
        if(!Objects.nonNull(chapter)){
            chapter = new Chapter(Long.parseLong(id_manga), Integer.parseInt(chap),name,files.length,java.time.LocalDate.now(),java.time.LocalTime.now());
            if(listchapter.isEmpty()){
                chapter.setPrechap(0l);
                chapter.setNextchap(0l);
                chapter = chapterrepo.save(chapter);
                chapter.setNextchap(chapter.getId());
                chapter.setPrechap(chapter.getId());
                chapterrepo.save(chapter);
            }else{
                Chapter prechapter = chapterrepo.findNewChapter(Long.parseLong(id_manga));
                chapter.setPrechap(prechapter.getId());
                chapter.setNextchap(chapterrepo.findMaxId()+1);
                chapter = chapterrepo.save(chapter);
                chapter.setNextchap(chapter.getId());
                chapter = chapterrepo.save(chapter);
                prechapter.setNextchap(chapter.getId());
                chapterrepo.save(prechapter);
            }
            if(files != null){
                for(MultipartFile x:files){
                    String fileName = StringUtils.cleanPath(x.getOriginalFilename());
                    String uploadDir =  "manga/" + id_manga + "/" + chapter.getId();
                    FileUploadUtil.saveFile(uploadDir, fileName, x);
                    list.add(new Manga(chapter.getId(), fileName,java.time.LocalDate.now(),java.time.LocalTime.now()));
                }
                mangarepo.saveAll(list);
            } else{
                model.addAttribute("message","Image can't empty!!!");
                return "createchapter";
            }
        }else{
            model.addAttribute("message","Error!!!");
        }

        ListManga manga = listmangarepo.findByID(Long.parseLong(id_manga));
        manga.setCountchapter(manga.getCountchapter()+1);
        listmangarepo.save(manga);
        listchapter = chapterrepo.showAllChapterById(Long.parseLong(id_manga));
        model.addAttribute("chapters",listchapter);
        return "chaptermanager";
    }
    @RequestMapping("/addnextchapter")
    public String addNextchapter(HttpServletRequest request, Model model){
        String id_chapter = request.getParameter("id_chapter");
        Chapter chapter = chapterrepo.findChapteById(Long.parseLong(id_chapter));
        model.addAttribute("chapter",chapter);
        return "createnextchapter";
    }
    @RequestMapping("/addprevchapter")
    public String addPrevchapter(HttpServletRequest request, Model model){
        String id_chapter = request.getParameter("id_chapter");
        Chapter chapter = chapterrepo.findChapteById(Long.parseLong(id_chapter));
        model.addAttribute("chapter",chapter);
        return "createprevchapter";
    }

    public Chapter createNextChap(Long id_chapter_prev,Long id_manga){
        Chapter chapter_prev = chapterrepo.findChapteById(id_chapter_prev);
        Chapter chapter = new Chapter();
        if(chapter_prev.getNextchap() == chapter_prev.getId()){
            chapter = chapterrepo.save(new Chapter(id_manga, 0,"",0,
                    chapter_prev.getId(),0l,java.time.LocalDate.now(),java.time.LocalTime.now()));
            chapter.setNextchap(chapter.getId());
            chapter_prev.setNextchap(chapter.getId());
            chapterrepo.saveAll(Arrays.asList(chapter,chapter_prev));
        }else {
            chapter = chapterrepo.save(new Chapter(id_manga, 0,"",0,
                    chapter_prev.getId(),chapter_prev.getNextchap(),java.time.LocalDate.now(),java.time.LocalTime.now()));
            Chapter chapter_next = chapterrepo.findChapteById(chapter_prev.getNextchap());
            chapter_prev.setNextchap(chapter.getId());
            chapter_next.setPrechap(chapter.getId());
            chapterrepo.saveAll(Arrays.asList(chapter_next,chapter_prev));
        }
        return chapter;
    }
    public Chapter createPrevChap(Long id_chapter_next,Long id_manga){
        Chapter chapter_next = chapterrepo.findChapteById(id_chapter_next);
        Chapter chapter = new Chapter();
        if(chapter_next.getPrechap() == chapter_next.getId()){
            chapter = chapterrepo.save(new Chapter(id_manga,0,"",0,
                    0l,chapter_next.getId(),java.time.LocalDate.now(),java.time.LocalTime.now()));
            chapter.setPrechap(chapter.getId());
            chapter_next.setPrechap(chapter.getId());
            chapterrepo.saveAll(Arrays.asList(chapter,chapter_next));
        }else {
            chapter = chapterrepo.save(new Chapter(id_manga, 0,"",0,
                    chapter_next.getPrechap(),chapter_next.getId(),java.time.LocalDate.now(),java.time.LocalTime.now()));
            Chapter chapter_prev = chapterrepo.findChapteById(chapter_next.getPrechap());
            chapter_next.setPrechap(chapter.getId());
            chapter_prev.setNextchap(chapter.getId());
            chapterrepo.saveAll(Arrays.asList(chapter_next,chapter_prev));
        }
        return chapter;
    }

    public List<Chapter> listChapter(Long id_manga){
        List<Chapter> list = new ArrayList<>();
        Chapter chapter = chapterrepo.findFirstChapter(id_manga);
        while(chapter.getNextchap() != chapter.getId()){
            list.add(chapter);
            chapter = chapterrepo.findChapteById(chapter.getNextchap());
        }
        list.add(chapter);
        return list;
    }

    @PostMapping("createnextchapter")
    public String createnextchapter(HttpServletRequest request, Model model, @RequestParam("files") MultipartFile[] files,
                                    @ModelAttribute("id_chapter") String id_chapter) throws IOException {
        HttpSession session = request.getSession();
        String id_manga = session.getAttribute("id_manga").toString();
        List<Manga> list = new ArrayList<>();
        String name = request.getParameter("content");
        String chap = request.getParameter("number");
        model.addAttribute("id",session.getAttribute("id"));
        Chapter chapter = chapterrepo.findByNumberandID(Long.parseLong(id_manga),Integer.parseInt(chap));
        if(!Objects.nonNull(chapter)){
            if(files != null){
                chapter = createNextChap(Long.parseLong(id_chapter),Long.parseLong(id_manga));
                chapter.setContent(name);
                chapter.setNumber(Integer.parseInt(chap));
                chapter.setCountchapter(files.length);
                chapterrepo.save(chapter);
                for(MultipartFile x:files){
                    String fileName = StringUtils.cleanPath(x.getOriginalFilename());
                    String uploadDir =  "manga/" + id_manga + "/" + chapter.getId();
                    FileUploadUtil.saveFile(uploadDir, fileName, x);
                    list.add(new Manga(chapter.getId(), fileName,java.time.LocalDate.now(),java.time.LocalTime.now()));
                }
                mangarepo.saveAll(list);
                ListManga manga = listmangarepo.findByID(Long.parseLong(id_manga));
                manga.setCountchapter(manga.getCountchapter()+1);
                listmangarepo.save(manga);
            } else{
                model.addAttribute("message","Image can't empty!!!");
                model.addAttribute("chapter",chapterrepo.findChapteById(Long.parseLong(id_chapter)));
                return "createnextchapter";
            }
        }else{
            model.addAttribute("chapter",chapterrepo.findChapteById(Long.parseLong(id_chapter)));
            model.addAttribute("message","Error!!!");
            return "createnextchapter";
        }

        List<Chapter> list_chapter = listChapter(Long.parseLong(id_manga));
        model.addAttribute("chapters",list_chapter);
        return "chaptermanager";
    }

    @PostMapping("createprevchapter")
    public String createprevchapter(HttpServletRequest request, Model model, @RequestParam("files") MultipartFile[] files,
                                    @ModelAttribute("id_chapter") String id_chapter) throws IOException {
        HttpSession session = request.getSession();
        String id_manga = session.getAttribute("id_manga").toString();

        List<Manga> list = new ArrayList<>();
        String name = request.getParameter("content");
        String chap = request.getParameter("number");
        model.addAttribute("id",session.getAttribute("id"));
        Chapter chapter = chapterrepo.findByNumberandID(Long.parseLong(id_manga),Integer.parseInt(chap));
        if(!Objects.nonNull(chapter)){
            if(files != null){
                chapter = createPrevChap(Long.parseLong(id_chapter),Long.parseLong(id_manga));
                chapter.setContent(name);
                chapter.setNumber(Integer.parseInt(chap));
                chapter.setCountchapter(files.length);
                chapterrepo.save(chapter);
                for(MultipartFile x:files){
                    String fileName = StringUtils.cleanPath(x.getOriginalFilename());
                    String uploadDir =  "manga/" + id_manga + "/" + chapter.getId();
                    FileUploadUtil.saveFile(uploadDir, fileName, x);
                    list.add(new Manga(chapter.getId(), fileName,java.time.LocalDate.now(),java.time.LocalTime.now()));
                }
                mangarepo.saveAll(list);
                ListManga manga = listmangarepo.findByID(Long.parseLong(id_manga));
                manga.setCountchapter(manga.getCountchapter()+1);
                listmangarepo.save(manga);
            } else{
                model.addAttribute("chapter",chapterrepo.findChapteById(Long.parseLong(id_chapter)));
                model.addAttribute("message","Image can't empty!!!");
                return "createprevchapter";
            }
        }else{
            model.addAttribute("chapter",chapterrepo.findChapteById(Long.parseLong(id_chapter)));
            model.addAttribute("message","Error!!!");
            return "createprevchapter";
        }

        List<Chapter> list_chapter = listChapter(Long.parseLong(id_manga));
        model.addAttribute("chapters",list_chapter);
        return "chaptermanager";
    }
}
