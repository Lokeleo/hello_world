package com.example.demo.controller;

import com.example.demo.Entity.*;
import com.example.demo.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Controller
public class MangaController {
	@Autowired
	private PictureMangaRepository mangarepo;
	@Autowired
	private MangaRepository listmangarepo;
	@Autowired
	private ChapterRepository chapterrepo;
	@Autowired
	Type_mangaRepository type_mangaRepository;
	@Autowired
	private TypeRepository typerepo;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private FavoriteRepository favoriteRepository;
	@Autowired
	HistoryRepository historyRepository;

	@RequestMapping("/readmanga")
	public String readmanga( HttpServletRequest request, Model model){
		HttpSession session = request.getSession();
		List<ListManga> all_manga = listmangarepo.showAllManga();
		session.setAttribute("id_manga",null);
		model.addAttribute("list_manga",all_manga);
		List<Type> list_type = typerepo.showAllCategory();
		model.addAttribute("categories",list_type);
		if(session.getAttribute("id") != null){
			model.addAttribute("history_manga",showHistory(request));
			model.addAttribute("id",session.getAttribute("id").toString());
			return "listManga";
		}
		return "index";

	}

	List<ListManga> showHistory(HttpServletRequest request){
		HttpSession session = request.getSession();
		List<Long> list = historyRepository.findByIduser(Long.parseLong(session.getAttribute("id").toString()));
		List<ListManga> listManga = new ArrayList<ListManga>();
		for(Long x:list){
			listManga.add(listmangarepo.findByID(x));
		}
		return listManga;
	}

	@RequestMapping("/search_manga")
	public String searchManga(HttpServletRequest request, Model model){
		String name = request.getParameter("search");
		List<ListManga> manga = listmangarepo.findManga(name);
		List<Type> list_type = typerepo.showAllCategory();
		model.addAttribute("list_manga",manga);
		model.addAttribute("categories",list_type);
		model.addAttribute("history_manga",showHistory(request));
		return "listManga";
	}

	@RequestMapping("/hot")
	public String hotManga(Model model,HttpServletRequest request){
		HttpSession session = request.getSession();
		List<Type> list_type = typerepo.showAllCategory();
		model.addAttribute("categories",list_type);
		model.addAttribute("list_manga",listmangarepo.showHotManga());
		if(session.getAttribute("id")!=null){
			model.addAttribute("id",session.getAttribute("id"));
			model.addAttribute("history_manga",showHistory(request));
			return "listManga";
		}
		return "/index";

	}

	@RequestMapping(value="selectmanga")
	public String select_manga( HttpServletRequest request, Model model){
		HttpSession session = request.getSession();
		String id = request.getParameter("id_manga");
		ListManga manga = listmangarepo.findByID(Long.parseLong(id));
		User author = userRepo.findUserById(manga.getIDuser());
		if(session.getAttribute("id") != null){
			String id_user = session.getAttribute("id").toString();
			session.setAttribute("id_manga",id);
			History history = historyRepository.findByIduserAndIdManga(Long.parseLong(id_user),Long.parseLong(id));
			if(Objects.nonNull(history)){
				history.setDay(java.time.LocalDate.now());
				history.setTime(java.time.LocalTime.now());
				historyRepository.save(history);
			}else{
				if(Long.parseLong(id) != Long.parseLong(id_user)) {
					manga = listmangarepo.findByID(Long.parseLong(id));
					manga.setCountview(manga.getCountview() + 1);
					listmangarepo.save(manga);
				}
				historyRepository.save(new History(Long.parseLong(id_user),Long.parseLong(id), java.time.LocalDate.now(),java.time.LocalTime.now()));
			}
		}
		manga = listmangarepo.findByID(Long.parseLong(id));
		if(session.getAttribute("id")!=null){
			model.addAttribute("id",session.getAttribute("id"));
			Favorite favorite = favoriteRepository.findByIdUserAndIdManga(Long.parseLong(session.getAttribute("id").toString()),Long.parseLong(id));
			if(Objects.nonNull(favorite)){
				model.addAttribute("follow",1);
			}else{
				model.addAttribute("follow",0);
			}
		}
		List<Chapter> list = listChapter(Long.parseLong(id));

		model.addAttribute("manga",manga);
		model.addAttribute("chapters",list);
		model.addAttribute("author",author.getName());
		return "chapter";
	}

	@RequestMapping("selectchapter")
	public String selectChapter(HttpServletRequest request,Model model){
		HttpSession session = request.getSession();
		String idchapter = request.getParameter("id_chapter");

		List<Manga> list = mangarepo.showManga(Long.parseLong(idchapter));
		String id_manga;
		if(session.getAttribute("id_manga")!=null){
			id_manga = session.getAttribute("id_manga").toString();
		}else{
			id_manga = request.getParameter("id_manga");
		}

		List<Chapter> chapters = listChapter(Long.parseLong(id_manga));

		Chapter chap = chapterrepo.findByChapterandID(Long.parseLong(id_manga), Long.parseLong(idchapter));
		if(session.getAttribute("id") != null){
			model.addAttribute("id", session.getAttribute("id"));
		}
		model.addAttribute("idmanga", id_manga);
		model.addAttribute("idchapter", idchapter);
		model.addAttribute("mangas", list);
		model.addAttribute("chapter", chap);
		model.addAttribute("chapters", chapters);
		model.addAttribute("id",session.getAttribute("id"));
		return "read_manga";
	}
	public List<Chapter> listChapter(Long id_manga){
		List<Chapter> list = new ArrayList<>();
		Chapter chapter = chapterrepo.findFirstChapter(id_manga);
		if(Objects.nonNull(chapter)){
			while(chapter.getNextchap() != chapter.getId()){
				list.add(chapter);
				chapter = chapterrepo.findChapteById(chapter.getNextchap());
			}
			list.add(chapter);
		}
		return list;
	}

	@RequestMapping("/selectCategory")
	public String selectCategory(HttpServletRequest request,Model model){
		HttpSession session = request.getSession();

		String value = request.getParameter("id_category");
		List<Type> list_type = typerepo.showAllCategory();
		model.addAttribute("categories",list_type);
		if(!value.equals("0")){
			List<Long> list_id = type_mangaRepository.findByCategory(Long.parseLong(value));
			List<ListManga> list_manga = new ArrayList<>();
			for(Long x:list_id){
				list_manga.add(listmangarepo.findByID(x));
			}
			model.addAttribute("list_manga",list_manga);
		} else {
			List<ListManga> list_manga = listmangarepo.showAllManga();
			model.addAttribute("list_manga",list_manga);
		}
		if(session.getAttribute("id") != null){
			model.addAttribute("id", session.getAttribute("id"));
			model.addAttribute("history_manga",showHistory(request));
			return "listManga";
		}
		return "index";
	}

	@RequestMapping("favorite")
	public String favorite(HttpServletRequest request,Model model){
		HttpSession session = request.getSession();
		if(session.getAttribute("id") == null){
			model.addAttribute("message","You need login to user function!!!");
			model.addAttribute("user",new User());
			return "/login";
		}
		Long id = Long.parseLong(session.getAttribute("id").toString());
		model.addAttribute("id", session.getAttribute("id"));
		List<Type> list_type = typerepo.showAllCategory();
		model.addAttribute("categories",list_type);
		model.addAttribute("history_manga",showHistory(request));
		List<Long> list_id = favoriteRepository.findListMangaFavorite(id);
		List<ListManga> list_manga = new ArrayList<>();
		for(Long x:list_id){
			list_manga.add(listmangarepo.findByID(x));
		}
		model.addAttribute("list_manga",list_manga);
		return "listManga";
	}

	@RequestMapping("addtoFavorite")
	public String addtoFavorite(HttpServletRequest request, Model model){
		HttpSession session = request.getSession();
		Long id = Long.parseLong(session.getAttribute("id").toString());
		String id_manga = request.getParameter("id_manga");
		Favorite favorite = favoriteRepository.findByIdUserAndIdManga(id,Long.parseLong(id_manga));
		if(Objects.nonNull(favorite)){
			favorite.setDay(java.time.LocalDate.now());
			favorite.setTime(java.time.LocalTime.now());
			favoriteRepository.save(favorite);
		}else{
			favoriteRepository.save(new Favorite(id,Long.parseLong(id_manga),java.time.LocalDate.now(),java.time.LocalTime.now()));
		}
		select_manga(request, model);
		return "chapter";
	}

	@RequestMapping("deletefromFavorite")
	public String deletefromFavorite(HttpServletRequest request, Model model){
		HttpSession session = request.getSession();
		Long id = Long.parseLong(session.getAttribute("id").toString());
		String id_manga = request.getParameter("id_manga");
		Favorite favorite = favoriteRepository.findByIdUserAndIdManga(id,Long.parseLong(id_manga));
		if(Objects.nonNull(favorite)){
			favoriteRepository.deleteFavoriteByIdUserAndIdManga(id,Long.parseLong(id_manga));
		}
		select_manga(request, model);
		return "chapter";
	}
}