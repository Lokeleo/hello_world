package com.example.demo.controller;

import com.example.demo.Entity.Chapter;
import com.example.demo.Entity.ListManga;
import com.example.demo.Entity.Manga;
import com.example.demo.Entity.User;
import com.example.demo.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.Arrays;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private MangaRepository listmangarepo;
    @Autowired
    private FavoriteRepository favoriteRepository;
    @Autowired
    HistoryRepository historyRepository;
    @Autowired
    Type_mangaRepository type_mangaRepository;

    @Autowired
    private PictureMangaRepository mangarepo;
    @Autowired
    private ChapterRepository chapterrepo;
    public static void deleteDir(File file) {
        if (file.isDirectory()) {
            String[] files = file.list();
            for (String child : files) {
                File childDir = new File(file, child);
                if (childDir.isDirectory()) {
                    deleteDir(childDir);
                } else {
                    childDir.delete();
                }
            }
            if (file.list().length == 0) {
                file.delete();
            }

        } else {
            file.delete();
        }
    }
    @RequestMapping(value="Admin_user")
    public String Admin_user(HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        List<User> allUser = userRepo.showAllUser();
        model.addAttribute("list_user",allUser);
        model.addAttribute("id",session.getAttribute("id"));
        return "Admin_user";
    }

    @RequestMapping(value="Admin_manga")
    public String Admin_manga(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        List<ListManga> list_manga = listmangarepo.showAllManga();
        model.addAttribute("list",list_manga);
        if(session.getAttribute("id")==null){
            return "login";
        }
        return "Admin_manga";
    }

    @PostMapping(value = "uplevel")
    public String uppower(HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        if(session.getAttribute("id")==null){
            return "login";
        }
        String id = session.getAttribute("id").toString();
        User admin = userRepo.findUserById(Long.parseLong(id));
        User user = userRepo.findUserById(Long.parseLong(request.getParameter("id_user")));
        if(user.getID_p() <= 2 && admin.getID_p() > (user.getID_p()+1)){
            user.setID_p(user.getID_p()+1);
            userRepo.save(user);
        }
        List<User> allUser = userRepo.showAllUser();
        model.addAttribute("list_user",allUser);
        model.addAttribute("id",session.getAttribute("id"));
        return "Admin_user";
    }

    @PostMapping(value = "downlevel")
    public String downpower(HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        if(session.getAttribute("id")==null){
            return "login";
        }
        String id = session.getAttribute("id").toString();
        User admin = userRepo.findUserById(Long.parseLong(id));
        User user = userRepo.findUserById(Long.parseLong(request.getParameter("id_user")));
        if(admin.getID_p() > user.getID_p() && admin.getID_p() >= 0){
            user.setID_p(user.getID_p() - 1);
            userRepo.save(user);
        }
        List<User> allUser = userRepo.showAllUser();
        model.addAttribute("list_user",allUser);
        model.addAttribute("id",session.getAttribute("id"));
        return "Admin_user";
    }

    @PostMapping("delete_user")
    public String delete_User(HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        if(session.getAttribute("id")==null){
            return "login";
        }
        String id = session.getAttribute("id").toString();
        User admin = userRepo.findUserById(Long.parseLong(id));
        User user = userRepo.findUserById(Long.parseLong(request.getParameter("id_user")));
        String currentDirectory = System.getProperty("user.dir");
        if(admin.getID_p() > user.getID_p()){
            List<ListManga> listManga = listmangarepo.findByIDuser(user.getID());
            for(ListManga x:listManga){
                historyRepository.deleteHistoryByIdUserOrIdManga(user.getID(), x.getID());
                favoriteRepository.deleteFavoriteByIdUserOrIdManga(user.getID(), x.getID());
                listmangarepo.deleteMangaById(x.getID());
                type_mangaRepository.deleteByIdManga(x.getID());
                File file = new File(currentDirectory+"/manga/" + x.getID());
                deleteDir(file);
            }
            userRepo.deleteUserByAdmin(user.getID());
        }
        List<User> allUser = userRepo.showAllUser();
        model.addAttribute("list_user",allUser);
        model.addAttribute("id",session.getAttribute("id"));
        return "Admin_user";
    }

    @RequestMapping(value="selectmanga_admin")
    public String select_manga( HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        String id = request.getParameter("id_manga");
        List<Chapter> list = chapterrepo.showAllChapterById(Long.parseLong(id));
        session.setAttribute("id_manga",id);
        if(list.isEmpty()){
            model.addAttribute("message", "Not available yet");
        }
        model.addAttribute("id",session.getAttribute("id"));
        model.addAttribute("idmanga",id);
        model.addAttribute("chapters",list);
        return "Admin_chapter";
    }
    @RequestMapping("selectchapter_admin")
    public String selectChapter(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        String chapter = request.getParameter("id_chapter");
        if(chapter.equals("0")) {
            chapter = request.getParameter("test");
        }
        String id_manga = session.getAttribute("id_manga").toString();
        List<Manga> list = mangarepo.showManga(Long.parseLong(chapter));
        List<Chapter> chapters = chapterrepo.showAllChapterById(Long.parseLong(id_manga));
        Chapter chap = chapterrepo.findByChapterandID(Long.parseLong(id_manga), Long.parseLong(chapter));
        model.addAttribute("idmanga", id_manga);
        model.addAttribute("idchapter", chapter);
        model.addAttribute("mangas", list);
        model.addAttribute("chapter", chap);
        model.addAttribute("chapters", chapters);
        model.addAttribute("id",session.getAttribute("id"));
        return "setupchapter";
    }

    public void deletemanga(Long id_manga){
        listmangarepo.deleteMangaById(id_manga);
        List<Chapter> chapters = chapterrepo.showAllChapterById(id_manga);
        historyRepository.deleteHistoryByIdManga(id_manga);
        type_mangaRepository.deleteByIdManga(id_manga);
        for(Chapter x:chapters){
            deletechapter(x.getId());

        }
    }
    public void deletechapter(Long id_chapter){
        chapterrepo.deleteChapterById(id_chapter);
        List<Manga> list = mangarepo.showManga(id_chapter);
        for(Manga x:list){
            deleteonepage(x.getID());
        }
    }

    public void deleteonepage(Long id_page){

        mangarepo.deleteOnepageById(id_page);
    }
    @PostMapping("/delete_chapter_admin")
    public String deleteChapterByAuthor(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        String id_delete = request.getParameter("id_chapter");
        String id_manga = session.getAttribute("id_manga").toString();
        Chapter chapter = chapterrepo.findChapteById(Long.parseLong(id_delete));

        String currentDirectory = System.getProperty("user.dir");
        File file = new File(currentDirectory + "/manga/" + id_manga+"/"+id_delete);
        deleteDir(file);
        if(chapter.getId() == chapter.getPrechap()){
            Chapter chapter_First = chapterrepo.findChapteById(chapter.getNextchap());
            chapter_First.setPrechap(chapter_First.getId());
            chapterrepo.save(chapter_First);
        } else if (chapter.getId() == chapter.getNextchap()){
            Chapter chapter_Last = chapterrepo.findChapteById(chapter.getPrechap());
            chapter_Last.setNextchap(chapter_Last.getId());
            chapterrepo.save(chapter_Last);
        } else {
            Chapter chapter_next = chapterrepo.findChapteById(chapter.getNextchap());
            Chapter chapter_prev = chapterrepo.findChapteById(chapter.getPrechap());
            chapter_next.setPrechap(chapter_prev.getId());
            chapter_prev.setNextchap(chapter_next.getId());
            chapterrepo.saveAll(Arrays.asList(chapter_prev,chapter_next));
        }
        deletechapter(Long.parseLong(id_delete));
        ListManga manga = listmangarepo.findByID(chapter.getIdmanga());
        manga.setCountchapter(manga.getCountchapter()-1);
        listmangarepo.save(manga);
        select_manga(request,model);
        return "Admin_chapter";
    }
    @PostMapping("/delete_manga_admin")
    public String deleteMangaByAuthor(HttpServletRequest request,Model model){
        String currentDirectory = System.getProperty("user.dir");
        String id_delete = request.getParameter("id");
        File file = new File(currentDirectory+"/manga/"+id_delete);
        deleteDir(file);
        deletemanga(Long.parseLong(id_delete));
        Admin_manga(request,model);
        return "Admin_manga";
    }
}
