package com.example.demo.Service;

import com.example.demo.Entity.User;
import com.example.demo.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
@Autowired
private UserRepository repo;

    public User login(String username, String password) {
        User user = repo.findByUsernameAndPassword(username, password);
        return user;
    }
    public User checkEmail(String email){
        User user = repo.findByEmail(email);
        return user;
    }
    public User checkUser(String username){
        User user = repo.findByUsername(username);
        return user;
    }

    public void updateResetPasswordToken(String token, String email) throws UserNotFoundException {
        User user = repo.findByEmail(email);

        if(user != null){
            user.setResetPasswordToken(token);
            repo.save(user);
        }else throw new UserNotFoundException("Could not find any user " + email);
    }

    public User findUser(Long id){
        User user = repo.findByID(id);
        return user;
    }

    public User get(String resetPasswordToken){
        return repo.findByResetPasswordToken(resetPasswordToken);
    }

    public void updatePassword(User user, String newPassword){
        user.setPassword(newPassword);
        user.setResetPasswordToken(null);
        repo.save(user);
    }

    public void set_new_password(User user, String newPasword){
        user.setNew_password(newPasword);
        repo.save(user);
    }
}