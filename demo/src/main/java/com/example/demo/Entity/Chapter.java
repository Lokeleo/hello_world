package com.example.demo.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name="chapter")
public class Chapter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long idmanga;
    private int number;
    private String content;
    private int countchapter;
    private Long prechap;
    private Long nextchap;
    private LocalDate daycreate;
    private LocalTime timecreate;

    public Chapter(Long idmanga, int number, String content, int countchapter, LocalDate daycreate, LocalTime timecreate) {
        this.idmanga=idmanga;
        this.number=number;
        this.content=content;
        this.countchapter = countchapter;
        this.daycreate = daycreate;
        this.timecreate = timecreate;
    }

    public Chapter(Long idmanga, int number, String content, int countchapter,Long prechap ,Long nextchap, LocalDate daycreate, LocalTime timecreate) {
        this.idmanga=idmanga;
        this.number=number;
        this.content=content;
        this.countchapter = countchapter;
        this.daycreate = daycreate;
        this.timecreate = timecreate;
        this.prechap=prechap;
        this.nextchap=nextchap;
    }
    public Chapter(){}

    public Long getId() {
        return id;
    }

    public Long getIdmanga() {
        return idmanga;
    }

    public int getCountchapter() {
        return countchapter;
    }

    public void setCountchapter(int countchapter) {
        this.countchapter = countchapter;
    }

    public LocalDate getDaycreate() {
        return daycreate;
    }

    public void setDaycreate(LocalDate daycreate) {
        this.daycreate = daycreate;
    }

    public LocalTime getTimecreate() {
        return timecreate;
    }

    public void setTimecreate(LocalTime timecreate) {
        this.timecreate = timecreate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Long getPrechap() {
        return prechap;
    }

    public void setPrechap(Long prechap) {
        this.prechap = prechap;
    }

    public Long getNextchap() {
        return nextchap;
    }

    public void setNextchap(Long nextchap) {
        this.nextchap = nextchap;
    }
}
