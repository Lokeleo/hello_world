package com.example.demo.Entity;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long ID;
    @NotBlank
    @Size(min=4,max=20,message = "Name should have at least 4 characters and no more than 20 characters!!!")
    private String name;
    @NotBlank
    @Size(min=6,max=20, message = "UserName should have at least 6 characters and no more than 20 characters!!!")
    private String username;
    @NotBlank
    @Size(min = 6,max=20,message = "Password should have at least 2 characters and no more than 20 characters!!!")
    private String password;
    private String new_password;
    @Column(name = "resetpasswordtoken")
    private String resetPasswordToken;
    private String gender;
    @NotBlank
    @Email
    private String email;
    @NotBlank
    private String address;
    @NotBlank
    private String birthday;
    private int ID_p;
    @Column(name ="photos",nullable = true, length = 64)
    private String photos;


    public User(){

    }
    public User(Long ID, String name, String username, String password, String birthday, int ID_p) {
        this.ID = ID;
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthday = birthday;
        this.ID_p = ID_p;
    }

    public Long getID(){
        return ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getID_p() {
        return ID_p;
    }

    public void setID_p(int ID_p) {
        this.ID_p = ID_p;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getPhotos() {
        if (photos.equals("") || getID() == null) {return "user-photos/default/user_default.png";}

        return "/user-photos/" + getID() + "/" + photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }
}
