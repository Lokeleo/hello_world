package com.example.demo.Entity;

import org.springframework.data.jpa.repository.Query;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
@Entity
@Table(name="listmanga")
public class ListManga {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ID;
    private Long IDuser;
    private String name;
    private String content;
    private int countchapter;
    private int count_view;
    private String image;
    private LocalDate daycreate;
    private LocalTime timecreate;

    public ListManga(Long IDuser, String name, int countchapter, String image, String content, LocalDate daycreate, LocalTime timecreate) {
        this.IDuser = IDuser;
        this.name = name;
        this.countchapter=countchapter;
        this.image = image;
        this.content = content;
        this.daycreate = daycreate;
        this.timecreate = timecreate;
    }

    public ListManga() {
    }

    public Long getID() {
        return ID;
    }

    public Long getIDuser() {
        return IDuser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return "/manga/" + getID() + "/" +image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getDaycreate() {
        return daycreate;
    }

    public void setDaycreate(LocalDate daycreate) {
        this.daycreate = daycreate;
    }

    public LocalTime getTimecreate() {
        return timecreate;
    }

    public void setTimecreate(LocalTime timecreate) {
        this.timecreate = timecreate;
    }

    public int getCountchapter() {
        return countchapter;
    }

    public void setCountchapter(int countchapter) {
        this.countchapter = countchapter;
    }

    public int getCountview() {
        return count_view;
    }

    public void setCountview(int countview) {
        this.count_view = countview;
    }
}
