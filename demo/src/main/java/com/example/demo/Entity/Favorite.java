package com.example.demo.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "favorite")
public class Favorite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long iduser;
    private Long idmanga;
    private LocalDate day;
    private LocalTime time;

    public Favorite(Long iduser, Long idmanga, LocalDate day, LocalTime time) {
        this.iduser = iduser;
        this.idmanga = idmanga;
        this.day = day;
        this.time = time;
    }
    public Favorite(){}

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
