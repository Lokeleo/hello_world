package com.example.demo.Entity;

import javax.persistence.*;

@Entity
@Table(name="type")
public class Type {
    @Id
    private Long id;
    private String type_name;

    public String getName() {
        return type_name;
    }

    public Long getID() {
        return id;
    }
}
