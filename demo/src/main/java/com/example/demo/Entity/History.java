package com.example.demo.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name="history")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long iduser;
    private Long idmanga;
    private LocalDate day;
    private LocalTime time;

    public History(Long iduser, Long idmanga, LocalDate day, LocalTime time) {
        this.iduser = iduser;
        this.idmanga = idmanga;
        this.day = day;
        this.time = time;
    }
    public History(){
    }

    public Long getIduser() {
        return iduser;
    }

    public void setIduser(Long iduser) {
        this.iduser = iduser;
    }

    public Long getIdmanga() {
        return idmanga;
    }

    public void setIdmanga(Long idmanga) {
        this.idmanga = idmanga;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
