package com.example.demo.Entity;

import javax.persistence.*;

@Entity
@Table(name="type_manga")
public class Type_manga {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long idmanga;
    private Long idtype;

    public Type_manga(Long idmanga, Long idtype) {
        this.idmanga = idmanga;
        this.idtype = idtype;
    }

    public Type_manga() {
    }

    public Long getIdmanga() {
        return idmanga;
    }

    public void setIdmanga(Long idmanga) {
        this.idmanga = idmanga;
    }

    public Long getIdtype() {
        return idtype;
    }

    public void setIdtype(Long idtype) {
        this.idtype = idtype;
    }
}
