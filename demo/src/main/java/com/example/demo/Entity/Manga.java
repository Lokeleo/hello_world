package com.example.demo.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name="manga")
public class Manga {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ID;
    private Long Idchapter;
    private String link;

    private LocalDate daycreate;
    private LocalTime timecreate;

    public Manga() {
    }

    public Manga(Long Idchapter,String link, LocalDate date, LocalTime time) {
        this.Idchapter=Idchapter;
        this.link = link;
        this.daycreate = date;
        this.timecreate = time;
    }

    public Long getID() {
        return ID;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink(String idmanga){
        return "/manga/" + idmanga + "/" + this.Idchapter + "/" + this.link;
    }

    public LocalDate getDate() {
        return daycreate;
    }

    public LocalTime getTime() {
        return timecreate;
    }
}
